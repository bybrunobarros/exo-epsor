const { gql } = require("apollo-server");

const typeDefs = gql`
  type Product {
    uuid: String!
    name: String!
    price: Float!
    type: String!
    enable: Boolean!
  }

  enum UserRole {
    COMMON
    ADMIN
  }

  type User {
    uuid: String!
    email: String!
    password: String!
    role: UserRole!
  }

  type Error {
    code: String!
    message: String!
  }

  type Query {
    products: [Product]
  }

  type Mutation {
    login(input: loginInput!): LoginPayload
    createProduct(input: createProductInput!): ProductPayload
    updateProduct(input: updateProductInput!): ProductPayload
    removeProduct(input: removeProductInput!): RemoveProductPayload
  }

  input loginInput {
    email: String!
    password: String!
  }

  type LoginPayload {
    token: String
    errors: [Error]
  }

  input createProductInput {
    name: String!
    price: Float!
    type: String!
    enable: Boolean!
  }

  input updateProductInput {
    uuid: String!
    name: String!
    price: Float!
    type: String!
    enable: Boolean!
  }

  input removeProductInput {
    uuid: String!
  }

  type ProductPayload {
    uuid: String
    name: String
    price: Float
    type: String
    enable: Boolean
    errors: [Error]
  }

  type RemoveProductPayload {
    state: String
    errors: [Error]
  }
`;

module.exports = typeDefs;
