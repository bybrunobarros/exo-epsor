const { ApolloServer } = require("apollo-server");
const resolvers = require("./resolver");
const typeDefs = require("./schema");
const { findUserFromToken } = require("./service/user");

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req }) => {
    const token = req.headers.authorization || "";
    const user = findUserFromToken(token);
    return { user };
  }
});

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
