# Bruno Barros' technical test


## Installing / Getting started
```shell
git clone git@bitbucket.org:bybrunobarros/exo-epsor.git
cd exo-epsor
npm i
npm start
```
It will start a graphQL playground on http://localhost:4000/.

## Some useful commands
First of all you should login first by calling the `login mutation
```graphql
mutation login($input: loginInput!){
  login(input: $input) {
    token
    errors {
      code
      message
    }
  }
}
```
Two different users are already set up. To identify as one of them set the query variables like this
```json
{
  "input": {
    "email": "common@example.com",
    "password": "common"
  }
}
```
or
```json
{
  "input": {
    "email": "admin@example.com",
    "password": "admin"
  }
}
```
Then get the JSON Web token to set it in the HTTP header before running the other commands
```json
{
  "authorization": "<your token here>"
}
```
 Other commands
 ```graphql
 query products {
   products {
     uuid
     name
     enable
   }
 }
 ```

```graphql
mutation createProduct($input: createProductInput!) {
  createProduct(input: $input) {
    uuid
    name
    price
    type
    enable
    errors {
      code
      message
    }
  }
}
```
with
```json
{
  "input": {
    "name": "Supa Dupa laptop",
    "price": 35,
    "type": "laptop",
    "enable": true
  }
}
```

```graphql
mutation updateProduct($input: updateProductInput!) {
  updateProduct(input: $input) {
    uuid
    name
    price
    type
    enable
    errors {
      code
      message
    }
  }
}
```
with
```json
{
  "input": {
    "uuid": "<uuid>",
    "name": "Change the laptop's name",
    "price": 35,
    "type": "laptop",
    "enable": true
  }
}
```
```graphql
mutation removeProduct($input: removeProductInput!) {
  removeProduct(input: $input) {
    state
    errors {
      code
      message
    }
  }
}
```
with
```json
{
  "input": {
    "uuid": "<uuid>"
  }
}
```

## Some notes
- The user management is at its bare minimum.
- I didn't add any unit tests because there is not much logic in here, I don't know where to add them.
