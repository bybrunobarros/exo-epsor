const { db } = require("../database");
const jwt = require("jsonwebtoken");
const SECRET_KEY = "ramdomKey";

function findUserFromToken(token) {
  if (!token) return null;

  const decoded = jwt.verify(token, SECRET_KEY);

  return db
    .get("users")
    .find({
      email: decoded.email
    })
    .value();
}

function isAuthenticated(user) {
  return !!user;
}

function isAdmin(user) {
  return isAuthenticated(user) && user.role === "ADMIN";
}

function login({ email, password }) {
  const user = db
    .get("users")
    .find({
      email: email,
      password: password
    })
    .value();

  if (!user) {
    return "";
  }

  return jwt.sign({ email: user.email }, SECRET_KEY);
}

module.exports = { findUserFromToken, isAdmin, isAuthenticated, login };
