const { db } = require("../database");
const uuidv4 = require("uuid/v4");

function create(body) {
  const product = { uuid: uuidv4(), ...body };
  db.get("products")
    .push(product)
    .write();

  return product;
}

function find(filter) {
  return db
    .get("products")
    .filter(filter)
    .value();
}

function remove(uuid) {
  const products = find({ uuid });
  if (products.length === 0) {
    return null;
  }

  db.get("products")
    .remove({ uuid })
    .write();

  return products[0];
}

function update(body) {
  const products = find({ uuid: body.uuid });
  if (products.length === 0) {
    return null;
  }

  db.get("products")
    .find({ uuid: body.uuid })
    .assign(body)
    .write();

  return body;
}

module.exports = { create, find, remove, update };
