let resolvers = {
  Mutation: {},
  Query: {}
};
const resolversByDomain = require("require-dir")();

for (const [key, domain] of Object.entries(resolversByDomain)) {
  resolvers = {
    Mutation: { ...resolvers.Mutation, ...domain.Mutation },
    Query: { ...resolvers.Query, ...domain.Query }
  };
}

module.exports = resolvers;
