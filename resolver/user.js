const { login } = require("../service/user");

module.exports = {
  Mutation: {
    login: (parent, { input }, context, info) => {
      const token = login({
        email: input.email,
        password: input.password
      });

      if (!token) {
        return {
          errors: [
            {
              code: "NOT_FOUND",
              message: "User not found with these credentials"
            }
          ]
        };
      }

      return { token };
    }
  }
};
