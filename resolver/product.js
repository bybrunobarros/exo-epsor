const { create, find, remove, update } = require("../service/product");
const { isAdmin, isAuthenticated } = require("../service/user");

function createNotAuthorizedPayload() {
  return {
    errors: [
      {
        code: "NOT_AUTHORIZED",
        message: "User not authorized"
      }
    ]
  };
}

function createNotFoundPayload() {
  return {
    errors: [
      {
        code: "NOT_FOUND",
        message: "Product not found"
      }
    ]
  };
}

module.exports = {
  Query: {
    products: (parent, args, context, info) => {
      if (!isAuthenticated(context.user)) {
        return null;
      }

      const filter = isAdmin(context.user) ? {} : { enable: true };
      return find(filter);
    }
  },
  Mutation: {
    createProduct: (parent, { input }, context, info) => {
      if (!isAdmin(context.user)) {
        return createNotAuthorizedPayload();
      }
      return create(input);
    },
    updateProduct: (parent, { input }, context, info) => {
      if (!isAdmin(context.user)) {
        return createNotAuthorizedPayload();
      }
      const product = update(input);
      if (!product) {
        return createNotFoundPayload();
      }

      return { ...product };
    },
    removeProduct: (parent, { input }, context, info) => {
      if (!isAdmin(context.user)) {
        return createNotAuthorizedPayload();
      }

      const product = remove(input.uuid);
      if (!product) {
        return createNotFoundPayload();
      }

      return { state: "removed" };
    }
  }
};
