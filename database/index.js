const lowdb = require("lowdb");
const data = require("./data");
const Memory = require("lowdb/adapters/Memory");

const db = lowdb(new Memory());
db.defaults(data).write();

module.exports = { db };
